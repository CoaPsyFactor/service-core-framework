#!/usr/bin/env node
const path = require('path');
const fs = require('fs');

const Service = require('../src/core/service/Service');

const [, , servicePath] = process.argv;

if (typeof servicePath !== 'string' || servicePath.length === 0) {
	throw new Error(`Path to the service must be provided`);
}

const actualServicePath = path.resolve(servicePath);

try {
	fs.accessSync(actualServicePath, fs.constants.R_OK);
} catch (error) {
	throw new Error(`Path "${servicePath}" is not accessible`);
}

const ServiceConstructor = require(actualServicePath);
let service = null;

try {
	service = new ServiceConstructor();
} catch (error) {
	throw new Error('Unable to instantiate the service')
}

if (service instanceof Service === false) {
	throw new Error('Given module does not provide proper service');
}

service.start().then(() => {
	console.log(`${ServiceConstructor.name} service started`);
}).catch((error) => {
	console.error(error);

	console.error(`Failed to start ${ServiceConstructor.name} service`);

	process.abort();
});
