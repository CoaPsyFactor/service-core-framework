const ServiceHandler = require('./ServiceHandler');
const StorageHandler = require('../storage/StorageHandler');
const AbstractClass = require('../../utils/AbstractClass');

class Service {
	static Health = {
		Ok: 'ok',
		Warning: 'warn',
		Error: 'error',
	};

	/** @type {{[name: string]: string}} */
	#erroredHandlers = {};

	/**
	 * @type {ServiceHandler[]}
	 */
	#handlers = null;

	/**
	 * @type {{[name: string]: StorageHandler}}
	 */
	#storageHandlers = null;

	/** @type {ORMRepository[]} */
	#repositories = null;

	/** @type {{ level: {'ok'|'warn'|'error'}, message: string }} */
	#serviceHealth = null;

	/**
	 * @returns {{ level: {'ok'|'warn'|'error'}, message: string }}
	 */
	get serviceHealth() {
		return { ...(this.#serviceHealth || { level: Service.Health.Error, message: 'waiting for input' }) };
	}

	/**
	 * @param {{ level: {'ok'|'warn'|'error'}, message: string }} input Service health input.
	 */
	set serviceHealth(input) {
		const { level = Service.Health.Error, message = 'waiting for input' } = typeof input === 'object' && input ? input : {};

		switch (level) {
			case Service.Health.Ok:
			case Service.Health.Warning:
			case Service.Health.Error:
				break;
			default:
				throw new TypeError(`Invalid health level "${level}"`);
		}

		this.#serviceHealth = { level, message };
	}

	/**
	 * @readonly
	 * @returns {ServiceHandler[]}
	 */
	get handlers() {
		return this.#handlers ? [...this.#handlers] : null;
	}

	/**
	 * @readonly
	 * @returns {{[name: string]: StorageHandler}}
	 */
	get storage() {
		return new Proxy(this, {
			get: (function GetServiceStorageHandler(_, handler) {
				if (this.#storageHandlers[handler] instanceof StorageHandler === false) {
					throw new TypeError(`Invalid storage handler "${handler}"`);
				}

				return this.#storageHandlers[handler];
			}).bind(this),
		});
	}

	/**
	 * @readonly
	 * @returns {{[name: string]: ORMRepository}}
	 */
	get repository() {
		const repositories = this.#repositories;

		return new Proxy(this, {
			get: (function ServiceGetRepository(_, repository) {
				const { [`${repository}`]: repositoryInstance = null } = repositories;

				if (repositoryInstance === null) {
					throw new TypeError(`Invalid repository object`);
				}

				return repositoryInstance;
			}).bind(this),
		});
	}

	/**
	 * @param {object} options Service options.
	 * @param {{handler: ServiceHandler, options: object}[]} options.handlers Service handlers.
	 * @param {{handler: StorageHandler, config: object}[]} options.storage Available service storage libs.
	 * @param {{repository: ORMRepository, storage: StorageHandler}[]} options.repositories Available service repositories.
	 */
	constructor({ handlers, storage = null, repositories = null }) {
		AbstractClass.call(this, Service);

		this.#storageHandlers = this.#prepareStorageHandlers(storage);
		this.#repositories = this.#prepareRepositories(repositories);
		this.#handlers = this.#prepareServiceHandlers(handlers);

		this.#bootServiceHandlers();
	}

	async start(options = {}) {
		await Promise.all([
			...Object.values(this.#storageHandlers || {}).map(async (storageHandler) => {
				try {
					await storageHandler.establishConnection();
				} catch (error) {
					this.#erroredHandlers[storageHandler.name] = error.message || JSON.stringify(error)
				}
			}),
			...this.handlers.map(async (handler) => {
				try {
					await handler.handle(options)
				} catch (error) {
					this.#erroredHandlers[handler.name] = error.message || JSON.stringify(error)
				}
			}),
		]);

		if (options.manualHealthStatusUpdate) {
			return;
		}

		setInterval(() => {
			this.updateServiceHealth();
		}, Number(process.env.SERVICE_HEALTH_CHECK_UPDATE_INTERVAL_SECONDS || 1) * 1000);
	}

	async stop(options) {
		await Promise.all([
			...this.handlers.map((handler) => handler.shutdown(options)),
			...Object.values(this.#storageHandlers).map((storageHandler) => storageHandler.closeConnection())
		]);
	}

	/**
	 * Updates service status based on storage handlers errors. (e.g dropped connection)
	 * 
	 * @returns {void}
	 */
	updateServiceHealth() {
		if (Object.keys(this.#erroredHandlers).length) {
			this.serviceHealth = { level: Service.Health.Error, message: { ...this.#erroredHandlers } };

			return;
		}

		this.serviceHealth = { level: Service.Health.Ok, message: null };
	}

	/**
	 * Perform boot over all registered handlers.
	 */
	#bootServiceHandlers() {
		/**
		 * @param {array} failedToBoot
		 * @param {ServiceHandler} handler
		 */
		this.#erroredHandlers = this.handlers.reduce((erroredHandlers, handler) => {
			try {
				handler.boot();
			} catch (error) {
				erroredHandlers[`${handler.name}`] = error.message || JSON.stringify(error);
			}

			return erroredHandlers
		}, this.#erroredHandlers);
	}

	/**
	 * Instantiate and boot all registered service handlers.
	 * 
	 * @param {ServiceHandler[]} handlers 
	 */
	#prepareServiceHandlers(handlers) {
		return (handlers || []).reduce((result, { handler: HandlerClass, options }) => {
			try {
				const serviceHandlerInstance = new HandlerClass({ service: this, options });

				if (serviceHandlerInstance instanceof ServiceHandler === false) {
					throw new TypeError(`Expected a instance of a ServiceHandler`);
				}

				if (serviceHandlerInstance.service instanceof Service === false) {
					throw new TypeError(`${HandlerClass.name} doesn't properly propagate service instance`);
				}

				return [...result, serviceHandlerInstance];
			} catch (error) {
				this.#erroredHandlers[HandlerClass.name] = error.message || JSON.stringify(error);
			}

			return [...result];
		}, []);
	}

	/**
	 * Instantiate, configure and monitor storage related handlers (mysql, mongo, redis etc).
	 * 
	 * @param {StorageHandler[]} storageHandlers 
	 */
	#prepareStorageHandlers(storageHandlers) {
		return (storageHandlers || []).reduce((result, { handler: HandlerClass, config }, index) => {
			/** @type {StorageHandler} */
			const storageHandler = new HandlerClass(config);

			if (storageHandler instanceof StorageHandler === false) {
				throw new TypeError(`Invalid storage handler on index "${index}" provided.`);
			}

			storageHandler.on('error', (error) => {
				this.#erroredHandlers[HandlerClass.name] = error.message || JSON.stringify(error);
			});

			storageHandler.on('disconnect', () => {
				this.#erroredHandlers[HandlerClass.name] = 'disconnected';
			});

			storageHandler.on('connect', () => {
				delete this.#erroredHandlers[HandlerClass.name];
			});

			return { ...result, [`${HandlerClass.name}`]: storageHandler };
		}, {});
	}

	/**
	 * Prepare and instantiate repositories setting respective storage handler for each.
	 * 
	 * @param {ORMRepository[]} repositories 
	 */
	#prepareRepositories(repositories) {
		return (repositories || []).reduce((result, { repository: RepositoryClass, storage: StorageHandlerClass, parameters = {} }) => {
			const storage = this.#storageHandlers[StorageHandlerClass.name] || null;

			if (storage === null) {
				throw new TypeError(`Invalid storage handler ${StorageHandlerClass.name}`);
			}

			return { ...result, [`${RepositoryClass.name}`]: new RepositoryClass({ ...(parameters || {}), storage }) };
		}, {});
	}
}

module.exports = Service;
