const { HttpServiceHandler, WebSocketServiceHandler } = require('./handlers');
const Service = require('./Service');
const ServiceHandler = require('./ServiceHandler');

module.exports = {
	Service,
	ServiceHandler,
	HttpServiceHandler,
	WebSocketServiceHandler,
};
