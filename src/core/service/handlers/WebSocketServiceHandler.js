const { WebSocketServer } = require('ws');
const ServiceHandler = require('../ServiceHandler');

class WebSocketServiceHandler extends ServiceHandler {
	#webSocketServer = null;
	#port = NaN;

	/**
	 * @readonly
	 * @returns {WebSocketServer}
	 */
	get webSocketServer() {
		return this.#webSocketServer;
	}

	/**
	 * @readonly
	 * @returns {Number}
	 */
	get port() {
		return Number(this.#port);
	}

	constructor({ service, options: { port = NaN } }) {
		super({ service, options: { port } });

		this.#webSocketServer = new WebSocketServer({ port });
	}

	boot() {
		const webSocketServiceHandler = this;

		this.webSocketServer.on('connection', async function OnWSClientConnection(client) {
			await webSocketServiceHandler.onClientConnect(client);

			/**
			 * @param {Buffer} data
			 */
			client.on('message', async (data) => {
				try {
					await webSocketServiceHandler.onClientMessage(client, JSON.parse(data.toString()));
				} catch (_) {
					await webSocketServiceHandler.onClientMessage(client, data.toString());
				}
			});
		});
	}

	async sendClientMessage(client, payload) {
		client.send(JSON.stringify(payload));
	}

	async onClientMessage(client, payload) { return true; }
	async onClientConnect(client) { return true; }
}

module.exports = WebSocketServiceHandler;