const express = require('express');
const bodyParser = require('body-parser');

const ServiceHandler = require('../ServiceHandler');

class HttpServiceHandler extends ServiceHandler {
	static HttpMethod = {
		Get: 'get',
		Post: 'post',
		Put: 'put',
		Patch: 'patch',
		Delete: 'delete',
		Options: 'options',
	};

	/** @type {express.Application} */
	#application = null;

	/** @type {express.Router} */
	#router = null;

	/** @type {number} */
	#port = NaN;

	/** @type {{[route: string]: Set}} */
	#availableRoutes = {};

	/** @type {string} */
	#prefix = null;

	/**
	 * @readonly
	 * @returns {express.Application}
	 */
	get application() {
		return this.#application;
	}

	/**
	 * @readonly
	 * @returns {Number}
	 */
	get port() {
		return Number(this.#port);
	}

	/**
	 * @readonly
	 * @returns {string}
	 */
	get name() {
		return `${this.constructor.name}`;
	}

	/**
	 * @readonly
	 * @returns {express.Router}
	 */
	get router() {
		return this.#router;
	}

	/**
	 * @readonly
	 * @returns {string}
	 */
	get prefix() {
		return `${this.#prefix}`;
	}

	constructor({ service, options: { port, prefix } }) {
		super({ service, options: { port } });

		if (Number.isNaN(port) || port < 1024) {
			throw new TypeError(`Invalid port value ${port}`);
		}

		this.#port = Number(port);
		this.#prefix = `${prefix}`
		this.#application = express();
		this.#router = express.Router();
	}

	boot() {
		this.application.use(bodyParser.json({ strict: true }));
		this.application.use(`/${this.prefix}`, this.router);

		this.application.listen(this.port, () => {
			this.get('/health-check', (_, respond) => {
				respond({ handler: `${this.name}`, ...(this.service.serviceHealth || {}) });
			});

			console.log(`${this.name} listening on port ${this.port}`);
		});
	}

	#addOptionsMethod(route, method) {
		if (this.#availableRoutes[route] instanceof Set === false) {
			this.#availableRoutes[route] = new Set();

			this.router.options(route, (_, response) => { response.json([...this.#availableRoutes[route]]) });
		}

		this.#availableRoutes[route].add(method.toUpperCase());
	}

	#route(method, route, handler) {
		if (typeof this.router[method] !== 'function') {
			throw new TypeError(`Invalid http method "${method}"`);
		}

		this.#addOptionsMethod(route, method);

		this.router[method](
			route,
			async (request, response) => {
				try {
					await handler(
						request,
						(data = null, status = 200) => {
							const numericStatus = typeof status === 'number' ? status : 200;

							response.status(numericStatus);

							if (data) {
								response.json(data);
							} else {
								response.send();
							}
						}, this) || {};
				} catch (error) {
					if (error.httpStatus) {
						return response.status(Number(error.httpStatus)).json({ message: error.message || 'Unknown error' });
					}

					console.error(error);

					return response.status(500).send();
				}
			}
		);
	}

	get(route, handler) {
		this.#route(HttpServiceHandler.HttpMethod.Get, route, handler);
	}

	post(route, handler) {
		this.#route(HttpServiceHandler.HttpMethod.Post, route, handler);
	}

	put(route, handler) {
		this.#route(HttpServiceHandler.HttpMethod.Put, route, handler);
	}

	patch(route, handler) {
		this.#route(HttpServiceHandler.HttpMethod.Patch, route, handler);
	}

	delete(route, handler) {
		this.#route(HttpServiceHandler.HttpMethod.Delete, route, handler);
	}
}

module.exports = HttpServiceHandler;