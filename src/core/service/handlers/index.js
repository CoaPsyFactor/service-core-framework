const HttpServiceHandler = require('./HttpServiceHandler');
const WebSocketServiceHandler = require('./WebSocketServiceHandler');

module.exports = {
	HttpServiceHandler,
	WebSocketServiceHandler,
};
