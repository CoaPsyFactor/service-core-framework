const AbstractClass = require('../../utils/AbstractClass');

class ServiceHandler {
	/** @type {object} */
	#options = null;

	/** @type {Service} */
	#service = null;

	/**
	 * @returns {Service}
	 */
	get service() {
		return this.#service;
	}

	/**
	 * @readonly
	 * @returns {string}
	 */
	get name() {
		return `${this.constructor.name}`;
	}

	/**
	 * @readonly
	 * @returns {object}
	 */
	get options() {
		return { ...(this.#options || {}) };
	}

	/**
	 * @readonly
	 * @returns {[storage: string]: StorageHandler}
	 */
	get storage() {
		return new Proxy(this, {
			get: (function ServiceHandlerGetStorage(_, storage) {
				return this.service.storage[storage];
			}).bind(this),
		});
	}

	/**
	 * @readonly
	 * @returns {{[name: string]: ORMRepository}}
	 */
	get repository() {
		return new Proxy(this, {
			get: (function ServiceHandlerGetRepository(_, name) {
				return this.service.repository[name];
			}).bind(this),
		});
	}

	constructor({ service, options = {} }) {
		AbstractClass.call(this, ServiceHandler);

		this.#options = options || {};
		this.#service = service;
	}

	boot(bootOptions) {
		throw new Error(`${this.constructor.name} doesn't implement boot method.`);
	}

	async handle(data) {
		throw new Error(`${this.constructor.name} doesn't implement handle method.`);
	}

	async shutdown(options) {
	}
}

module.exports = ServiceHandler;
