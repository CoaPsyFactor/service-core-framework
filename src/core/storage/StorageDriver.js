const { EventEmitter } = require('events');

class StorageDriver {

	/** @type {object} */
	#config = null;

	/** @type {EventEmitter} */
	#eventEmitter = null;

	/**
	 * @readonly
	 * @returns {EventEmitter}
	 */
	get eventEmitter() {
		return this.#eventEmitter;
	}

	/**
	 * @readonly
	 * @returns {object}
	 */
	get config() {
		return { ...(this.#config || {}) };
	}

	constructor(config, eventEmitter) {
		this.#config = config;

		if (eventEmitter instanceof EventEmitter) {
			this.#eventEmitter = eventEmitter;
		} else {
			this.#eventEmitter = new EventEmitter();
		}
	}

	parse(data) {
		throw new Error(`${this.constructor.name} doesn't implement "parse"`);
	}

	async execute(data) {
		throw new Error(`${this.constructor.name} doesn't implement "execute"`);
	}

	async connect() {
		throw new Error(`${this.constructor.name} doesn't implement "connect"`);
	}

	async disconnect() {
		throw new Error(`${this.constructor.name} doesn't implement "disconnect"`);
	}

}

module.exports = StorageDriver;
