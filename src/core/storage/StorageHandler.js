const { EventEmitter } = require('events');

/**
 * @typedef {{[property: string]: any}} StorageHandlerConfig
 */

const AbstractClass = require('../../utils/AbstractClass');
const StorageDriver = require('./StorageDriver');

/**
 * @typedef {object} StorageHandlerParameters
 * 
 * @property {string} name Handler name.
 * @property {StorageHandlerConfig} config Handler config.
 * @property {StorageDriver} driver Handler driver.
 */
class StorageHandler {
	static Event = {
		Connect: 'connect',
		Disconnect: 'disconnect',
		Error: 'error',
		Query: 'query',
	};

	/**
	 * @type {StorageDriver}
	 * @private
	 */
	#driver = null;

	/**
	 * @type {StorageHandlerConfig}
	 * @private
	 */
	#config = null;

	/**
	 * @type {EventEmitter}
	 * @private
	 */
	#eventEmitter = null;

	/**
	 * @readonly
	 * @returns {string}
	 */
	get name() {
		return `${this.constructor.name}`;
	}

	/**
	 * @readonly
	 * @returns {StorageHandlerConfig}
	 */
	get config() {
		return { ...(this.#config || {}) };
	}

	/**
	* @param {StorageHandlerParameters} options
	*/
	constructor(options) {
		AbstractClass.call(this, StorageHandler);

		const { config = null, driver = null } = options;

		try {
			this.#config = this.parseConfig(config);
		} catch (error) {
			throw new TypeError(`${this.constructor.name} has invalid "config" property provided. ${error.message}`);
		}

		this.#eventEmitter = new EventEmitter();

		const storageDriver = new driver(this.config, this.#eventEmitter);

		if (storageDriver instanceof StorageDriver === false) {
			throw new TypeError(`${this.constructor.name} has invalid "driver" property provided.`);
		}

		this.#driver = storageDriver;
	}


	/**
	 * Register new event listener.
	 * 
	 * @param {'connect'|'disconnect'|'error'|'query'} event 
	 * @param {Function} handler 
	 * @returns {Function} Call this to remove listener
	 */
	on(event, handler) {
		const eventName = `${event}`.toLowerCase();

		switch (eventName) {
			case StorageHandler.Event.Connect:
			case StorageHandler.Event.Disconnect:
			case StorageHandler.Event.Error:
			case StorageHandler.Event.Query:
				break;
			default:
				throw new TypeError(`Invalid event "${event}"`);
		}

		if (typeof handler !== 'function') {
			throw new TypeError('Invalid handler.');
		}

		this.#eventEmitter.on(eventName, handler);

		return (function RemoveStorageListenerEvent() {
			this.#eventEmitter.off(eventName, handler);
		}).bind(this);
	}

	parseConfig(config) {
		throw new Error(`${this.constructor.name} doesn't implement "parseConfig"`);
	}

	async establishConnection() {
		try {
			await this.#driver.connect(this.config);
		} catch (error) {
			this.#eventEmitter.emit(StorageHandler.Event.Error, error);

			return
		}
	}

	async closeConnection() {
		try {
			await this.#driver.disconnect();
		} catch (error) {
			this.#eventEmitter.emit(StorageHandler.Event.Error, error);

			return;
		}

		this.#eventEmitter.emit(StorageHandler.Event.Disconnect);
	}

	async query(options) {
		try {
			const data = this.#driver.parse(options);
			const results = await this.#driver.execute(data);

			this.#eventEmitter.emit(StorageHandler.Event.Query, results, options);

			return results;
		} catch (error) {
			this.#eventEmitter.emit(StorageHandler.Event.Error, error);
		}

		return null;
	}

	async fetch(options) {
		return this.query(options);
	}

	async store(options) {
		return this.query(options);
	}

	async delete(options) {
		return this.query(options);
	}
}

module.exports = StorageHandler;
