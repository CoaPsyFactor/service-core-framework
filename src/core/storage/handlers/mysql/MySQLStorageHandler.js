const StorageHandler = require('../../StorageHandler');
const MySQLStorageDriver = require('./MySQLStorageDriver');

class MySQLStorageHandler extends StorageHandler {
	constructor(config) {
		super({ config, driver: MySQLStorageDriver });
	}

	parseConfig(config) {
		const { host = null, port = null, user = null, password = null, database = null } = config || {};

		if (typeof database !== 'string' || database.length === 0) {
			throw new TypeError(`Invalid database provided "${JSON.stringify(database)}"`);
		}

		return {
			host: typeof host === 'string' && host ? host : 'localhost',
			port: typeof port === 'number' && Number.isNaN(port) === false ? port : 3306,
			user: typeof user === 'string' && user.length ? user : undefined,
			password: typeof password === 'string' && password.length ? password : undefined,
			database: typeof database === 'string' && database.length ? database : undefined,
		};
	}
}

module.exports = MySQLStorageHandler;
