const mysql = require('mysql');

const StorageDriver = require('../../StorageDriver');
const StorageHandler = require('../../StorageHandler');

class MySQLStorageDriver extends StorageDriver {
	/**
	 * @type {mysql.Connection}
	 */
	#connection = null;

	#isConnectionOffline() {
		return this.#connection === null || ['disconnected', 'protocol_error'].includes(this.#connection.state);
	}

	parse(query) {
		if (typeof query !== 'string') {
			throw new TypeError('Currently only string queries are allowed');
		}

		return `${query}`;
	}

	async execute(query) {
		if (this.#isConnectionOffline()) { // In case that connection is not established
			await this.connect(); // try connecting again

			if (this.#isConnectionOffline()) { // If its still not established
				// emit an event, and prevent "execute" from further processing.
				this.eventEmitter.emit(StorageHandler.Event.Error, new Error(`Connection state is "${this.#connection && this.#connection.state || 'not instantiate'}"`));

				return null;
			}
		}

		return new Promise((function MySQLStorageDriverExecute(resolve, reject) {
			this.#connection.query(query, (error, results) => {
				if (error) {
					reject(error);

					this.eventEmitter.emit(StorageHandler.Event.Error, error);

					return;
				}

				resolve(results);
			})
		}).bind(this));
	}

	async connect() {
		if (this.#isConnectionOffline() === false) {
			return;
		}

		if (this.#connection) {
			this.#connection.removeAllListeners();
		}

		this.#connection = mysql.createConnection({
			host: this.config.host || 'localhost',
			user: this.config.user || undefined,
			password: this.config.password || undefined,
			database: this.config.database,
			multipleStatements: true,
		});

		this.#connection.on('error', (error) => {
			this.eventEmitter.emit(StorageHandler.Event.Error, error);
		});

		this.#connection.on('disconnect', () => {
			this.eventEmitter.emit(StorageHandler.Event.Disconnect);
		});

		return new Promise((function MySQLStorageDriverConnect(resolve, reject) {
			this.#connection.connect((error) => {
				if (error) {
					this.#connection = null;

					this.eventEmitter.emit(StorageHandler.Event.Error, error);

					return reject(error);
				}

				this.eventEmitter.emit(StorageHandler.Event.Connect);

				return resolve(this);
			});
		}).bind(this));
	}

	async disconnect() {
		if (this.#connection.state !== 'connected') {
			return;
		}

		const connection = this.#connection;
		this.#connection = null;

		return new Promise((function MySQLStorageDriverDisconnect(resolve, reject) {
			connection.removeAllListeners();

			connection.end((error) => {
				if (error) {
					reject(error);

					this.eventEmitter.emit(StorageHandler.Event.Error, error);

					return;
				}

				this.eventEmitter.emit(StorageHandler.Event.Disconnect);

				resolve();
			});
		}).bind(this));
	}
}

module.exports = MySQLStorageDriver;