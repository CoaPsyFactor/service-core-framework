const MySQLStorageHandler = require('./mysql/MySQLStorageHandler');
const MongoDBStorageHandler = require('./mongodb/MongoDBStorageHandler');

module.exports = { MySQLStorageHandler, MongoDBStorageHandler };