const { MongoClient, Db } = require('mongodb');
const StorageDriver = require('../../StorageDriver');
const StorageHandler = require('../../StorageHandler');
const Commands = require('./Commands');

class MongoDBStorageDriver extends StorageDriver {
	/**
	 * @type {MongoClient}
	 */
	#client = null;

	/**
	 * @type {Db}
	 */
	#database = null;

	parse(data) {
		if (typeof data !== 'object' || data === null) {
			throw new TypeError('Invalid data object provided');
		}

		const { command = null, filters = null, payload = null, collection = null, options = {} } = data;

		if (typeof collection !== 'string' || collection.length === 0) {
			throw new TypeError(`Invalid collection name "${collection}"`);
		}

		const result = [collection, command];

		switch (command) {
			case Commands.Find:
			case Commands.FindOne:
			case Commands.FindOneAndDelete:
			case Commands.DeleteOne:
			case Commands.DeleteMany:
			case Commands.CountDocuments:
				return [...result, filters || {}, options];
			case Commands.FindOneAndReplace:
			case Commands.FindOneAndUpdate:
				return [...result, filters || {}, payload, options];
			case Commands.InsertOne:
			case Commands.InsertMany:
			case Commands.Aggregate:
			case Commands.BulkWrite:
				return [...result, payload, options];
			default:
				throw new TypeError(`Invalid command "${command}"`);
		};
	}

	async execute([collection, command, ...parameters]) {
		if (this.#database instanceof Db === false) {
			return null;
		}

		const collectionInstance = this.#database.collection(collection);
		const result = await collectionInstance[command](...parameters);

		if (typeof result.toArray === 'function') {
			return result.toArray();
		}

		return result;
	}

	async connect() {
		const {
			connectionSchema, host, port, database, connectionOptions
		} = this.config || {};

		if (this.#client) {
			this.#client.removeAllListeners();
		}

		this.#client = new MongoClient(`${connectionSchema}://${host}:${port}`);

		this.#client.on('error', (error) => {
			this.eventEmitter.emit(StorageHandler.Event.Error, error);
		});

		this.#client.on('timeout', () => {
			this.eventEmitter.emit(StorageHandler.Event.Error, new Error('Connection timed out'));
		});

		this.#client.on('serverHeartbeatFailed', (event) => {
			this.eventEmitter.emit(StorageHandler.Event.Error, event);
		});

		this.#client.on('connectionCreated', () => {
			this.eventEmitter.emit(StorageHandler.Event.Connect);
		});

		try {
			await this.#client.connect();

			this.eventEmitter.emit(StorageHandler.Event.Connect);

			this.#database = this.#client.db(database);
		} catch (error) {
			this.eventEmitter.emit(StorageHandler.Event.Error, error);

			throw error;
		}

		return this;
	}

	async disconnect() {
		if (this.#client) {
			this.#client.removeAllListeners();

			await this.#client.close();

			this.eventEmitter.emit(StorageHandler.Event.Disconnect);

			this.#client = null;
			this.#database = null;
		}
	}
}

module.exports = MongoDBStorageDriver;