
const StorageHandler = require('../../StorageHandler');
const Commands = require('./Commands');
const MongoDBStorageDriver = require('./MongoDBStorageDriver');

class MongoDBStorageHandler extends StorageHandler {
	static Commands = { ...Commands };

	static ConnectionSchema = {
		Standard: 'mongodb',
		DNSSpeedList: 'mongodb+srv'
	};

	constructor(config) {
		super({ config, driver: MongoDBStorageDriver });
	}

	parseConfig(config) {
		const {
			connectionSchema = MongoDBStorageHandler.ConnectionSchema.Standard,
			host = '127.0.0.1',
			port = 27017,
			database = null,
			connectionOptions = {}
		} = config || {};

		switch (connectionSchema) {
			case MongoDBStorageHandler.ConnectionSchema.Standard:
			case MongoDBStorageHandler.ConnectionSchema.DNSSpeedList:
				break;
			default:
				throw new TypeError(`Invalid connection schema "${connectionSchema}"`);
		}

		if (typeof database !== 'string' || database.length === 0) {
			throw new TypeError(`Invalid connection database ${database}`);
		}

		if (typeof port !== 'number' || Number.isNaN(port) || port < 0) {
			throw new TypeError(`Invalid connection port ${port}`);
		}

		return {
			connectionSchema: `${connectionSchema}`,
			host: `${host}`,
			port: Number(port),
			database: `${database}`,
			connectionOptions: { ...connectionOptions },
		};
	}
}

module.exports = MongoDBStorageHandler;
