class Commands {
	static Find = 'find';
	static FindOne = 'findOne';
	static FindOneAndDelete = 'findOneAndDelete';
	static FindOneAndReplace = 'findOneAndReplace';
	static FindOneAndUpdate = 'findOneAndUpdate';
	static DeleteOne = 'deleteOne';
	static DeleteMany = 'deleteMany';
	static InsertOne = 'insertOne';
	static InsertMany = 'insertMany';
	static Aggregate = 'aggregate';
	static BulkWrite = 'bulkWrite';
	static CountDocuments = 'countDocuments';
}

module.exports = Commands;
