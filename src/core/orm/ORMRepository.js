const AbstractClass = require('../../utils/AbstractClass');
const StorageHandler = require('../storage/StorageHandler');
const ORMEntity = require('./ORMEntity');

class ORMRepository {
	/** @type {StorageHandler} */
	#storage = null;

	/** @type {string} */
	#source = null;

	/** @type {Function} */
	#entity = null;

	/** @type {string */
	#primaryKey = null;

	/**
	 * @readonly
	 * @returns {string|null}
	 */
	get source() {
		return this.#source ? `${this.#source}` : null;
	}

	/**
	 * @readonly
	 * @returns {StorageHandler}
	 */
	get storage() {
		return this.#storage;
	}

	/**
	 * @readonly
	 * @returns {FunctionConstructor}
	 */
	get EntityClass() {
		return this.#entity;
	}

	/**
	 * @readonly
	 * @returns {string}
	 */
	get primaryKey() {
		return this.#primaryKey ? `${this.#primaryKey}` : null;
	}

	/**
	 * @param {object} options Repository options.
	 * @param {StorageHandler} options.storage Instance of StorageHandler.
	 * @param {string} options.source source name where repository is pointing.
	 * @param {Function} options.entity type of repository entity.
	 * @param {string} options.primaryKey Primary key of the collection
	 * @param {FunctionConstructor} [options.storageClass] Expected type of storage handler.
	 */
	constructor({ storage = null, source = null, entity: Entity = null, storageClass: StorageClass = null, primaryKey = 'id' }) {
		AbstractClass.call(this, ORMRepository);

		if (storage instanceof StorageHandler === false) {
			throw new TypeError('Expected StorageHandler.');
		}

		this.#storage = storage;

		if (StorageClass && this.storage instanceof StorageClass === false) {
			throw new TypeError(`${this.constructor.name} expects instance of "${StorageClass.name}" as storage engine. Got "${this.storage.constructor.name}" instead`);
		}

		if (typeof source !== 'string' || source.length === 0) {
			throw new TypeError(`Invalid source name provided.`);
		}

		this.#source = `${source}`;


		if (typeof primaryKey !== 'string' && primaryKey) {
			throw new TypeError(`Invalid primaryKey identifier "${primaryKey}"`);
		}

		this.#primaryKey = primaryKey;

		if (Entity === null) {
			throw new TypeError('Missing entity');
		}

		const entityInstance = new Entity();

		if (entityInstance instanceof ORMEntity === false) {
			throw new TypeError('Invalid entity class identifier provided, not implementing ORMEntity');
		}

		this.#entity = Entity;
	}

	/**
	 * Creates new instance of repos entity, also hydrating it.
	 * 
	 * @param {object} data Entity hydration data.
	 * @returns {ORMEntity|null}
	 */
	#getEntityInstance(data) {
		if (typeof data !== 'object' || data === null) {
			return null;
		}

		/** @type {ORMEntity} */
		const entity = new this.EntityClass();

		return entity.hydrate(data);
	}

	/**
	 * Retrieve execution command based on input data.
	 * 
	 * @param {'Fetch'|'Store'|'Delete'} commandType Type of a command that has to be retrieved.
	 * @param  {...any} input 
	 * @returns {array}
	 */
	#getCommand(commandType, ...input) {
		const GetCommand = this[`get${commandType}Command`];

		if (typeof GetCommand !== 'function') {
			throw new TypeError(`Invalid command type "${commandType}"`);
		}

		const command = GetCommand.call(this, ...input);

		return Array.isArray(command) ? command : [command];
	}

	/**
	 * Returns actual value of the primary field on given entity
	 * @param {ORMEntity} entity 
	 */
	getEntityPrimaryKeyValue(entity) {
		return entity[`${this.primaryKey}`];
	}

	/**
	 * Return actual command (object) that is used to execute store (saving).
	 * 
	 * @param  {...any} input 
	 */
	getStoreCommand(...input) {
		throw new TypeError(`${this.constructor.name} doesn't implement "getStoreCommand"`);
	}

	/**
	 * Return actual command (object) that is used to execute fetch.
	 * 
	 * @param  {...any} input 
	 */
	getFetchCommand(...input) {
		throw new TypeError(`${this.constructor.name} doesn't implement "getFetchCommand"`);
	}

	/**
	 * Return actual command (object) that is used to execute deletion (removal)
	 * 
	 * @param  {...any} input 
	 */
	getDeleteCommand(...input) {
		throw new TypeError(`${this.constructor.name} doesn't implement "getDeleteCommand"`);
	}

	/**
	 * Retrieve (fetch) from storage based on given input
	 * 
	 * @param {string|object} criteria Fetch criteria.
	 * @param  {...any} parameters 
	 * @returns {ORMEntity[]}
	 */
	async fetch(criteria, ...parameters) {
		const results = await this.storage.fetch(...this.#getCommand('Fetch', ...[criteria, ...parameters]));

		if (Array.isArray(results)) {
			return results.map((result) => this.#getEntityInstance(result));
		}

		if (results) {
			return [this.#getEntityInstance(results)];
		}

		return [];
	}

	/**
	 * Stores (saves) given entities to storage.
	 * 
	 * @param {ORMEntity|ORMEntity[]} entities Entities that should be stored (inserted or updated)
	 * @param  {...any} parameters 
	 * @returns {mixed}
	 */
	async store(entities, ...parameters) {
		const entitiesArray = (Array.isArray(entities) ? entities : [entities]).filter((entity) => entity);
		const data = entitiesArray.map((entity, index) => {
			if (entity instanceof this.EntityClass === false) {
				throw new TypeError(`Invalid entity type on index "${index}"`);
			}

			return entity;
		});

		return this.storage.store(...this.#getCommand('Store', ...[data, ...parameters]));
	}

	/**
	 * Performs deletion (removal) of provided entities.
	 * 
	 * @param {ORMEntity|ORMEntity[]} entities Entities that will be removed.
	 * @param  {...any} parameters 
	 * @returns {mixed}
	 */
	async delete(entities, ...parameters) {
		const entitiesArray = (Array.isArray(entities) ? entities : [entities]).filter((entity) => entity);

		if (entitiesArray.length === 0) {
			return;
		}

		const data = entitiesArray.map((entity, index) => {
			if (entity instanceof this.EntityClass === false) {
				throw new TypeError(`Invalid entity type on index "${index}"`);
			}

			return entity;
		});

		return this.storage.delete(...this.#getCommand('Delete', ...[data, ...parameters]));
	}
}

module.exports = ORMRepository;

