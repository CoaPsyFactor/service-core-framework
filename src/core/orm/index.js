const { MongoDBRepository, MySQLRepository } = require('./repositories');
const ORMEntity = require('./ORMEntity');
const ORMRepository = require('./ORMRepository');

module.exports = {
	MongoDBRepository,
	MySQLRepository,
	ORMEntity,
	ORMRepository,
};
