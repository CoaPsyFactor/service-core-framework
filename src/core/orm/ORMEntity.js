const AbstractClass = require('../../utils/AbstractClass');

class ORMEntity {
	/** @type {string[]} */
	#properties = null;

	/** @type {any[]} */
	#values = [];

	/**
	 * @readonly
	 * @returns {string[]}
	 */
	get properties() {
		return this.#properties ? [...this.#properties] : [];
	}

	/**
	 * @readonly
	 * @returns {{[field: string]: any}}
	 */
	get values() {
		return this.#values ? { ...this.#values } : {};
	}

	/** 
	 * @param {object} options Entity options.
	 * @param {Set<string>} options.properties Available entity properties.
	 * @param {{[property: string]: any}|null} [options.values=null] Values that will be hydrated.
	 */
	constructor({ properties = null, values = null }) {
		AbstractClass.call(this, ORMEntity);

		if (properties instanceof Set === false || properties.size === 0) {
			throw new TypeError(`"${this.constructor.name}" has invalid property list`);
		}

		this.#properties = [...properties].reduce((result, property, index) => {
			if (typeof property !== 'string' || property.length === 0) {
				throw new TypeError(`Invalid property name on index "${index}"`);
			}

			Object.defineProperty(this, property, {
				get: () => this.#values[property],
				set: (value) => this.#values[property] = value,
				enumerable: true, // We want to include this property when destructing or iterating
				configurable: false, // We want to prevent property of same name to be defined on entity itself.
			});

			return [...result, property];
		}, []);

		if (typeof values === 'object' && values !== null) {
			this.hydrate.call(this, values);
		}
	}

	/**
	 * Hydrate entity with actual data- also assign properties once hydration is done.
	 * 
	 * @param {object} input Input data.
	 * @returns {ORMEntity}
	 */
	hydrate(input) {
		this.#values = this.#properties.reduce((result, property) => {
			return { ...result, [`${property}`]: input[property] };
		}, {});

		return this;
	}
}

module.exports = ORMEntity;
