const MongoDBRepository = require('./MongoDBRepository');
const MySQLRepository = require('./MySQLRepository');

module.exports = { MongoDBRepository, MySQLRepository };
