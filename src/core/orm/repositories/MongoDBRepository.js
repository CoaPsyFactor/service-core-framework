const AbstractClass = require('../../../utils/AbstractClass');
const MongoDBStorageHandler = require('../../storage/handlers/mongodb/MongoDBStorageHandler');
const ORMEntity = require('../ORMEntity');
const ORMRepository = require('../ORMRepository');

/**
 * Usage of this repository we are limiting us to mysql
 * As any other storage handler will cause error to be thrown
 */
class MongoDBRepository extends ORMRepository {
	constructor(options) {
		super({ ...options, storageClass: MongoDBStorageHandler });

		AbstractClass.call(this, MongoDBRepository);
	}

	/**
	 * Generate `find` command with provided criteria.
	 * 
	 * @param {object} criteria 
	 * @param  {...any} parameters 
	 * @returns {[object, ...any]}
	 */
	getFetchCommand(criteria, ...parameters) {
		if (typeof criteria !== 'object' || criteria === null) {
			throw new TypeError('Invalid type of criteria provided');
		}

		const command = {
			filters: { ...criteria },
			collection: `${this.source}`,
			options: parameters[0],
			command: MongoDBStorageHandler.Commands.Find
		};

		return [command, ...parameters];
	}

	/**
	 * Generates command for entity removal- based on input
	 * it will generate either `DeleteOne` or `DeleteMany` command.
	 * It will also provide all required parameters in order for entities to be removed.
	 * 
	 * @param {ORMEntity|ORMEntity[]} documents 
	 * @param  {...any} parameters 
	 * @returns {[object, ...any]}
	 */
	getDeleteCommand(documents, ...parameters) {
		const inputData = Array.isArray(documents) ? documents : [documents];

		const { filters } = inputData.reduce((result, entity) => {
			const entityFilter = entity
				.properties
				.reduce((entityFilter, property) => ({ ...entityFilter, [`${property}`]: entity[`${property}`] }), {});

			result.filters.$or.push(entityFilter);

			return result;
		}, { filters: { $or: [] } });

		const command = {
			filters,
			command: inputData.size === 1 ? MongoDBStorageHandler.Commands.DeleteOne : MongoDBStorageHandler.Commands.DeleteMany,
			collection: `${this.source}`,
			options: parameters[0],
		};

		return [command, ...parameters];
	}

	/**
	 * Generates object of two properties, one represents inserting entities while the 
	 * other represents updating entities.
	 * Usage of this method is overriden with `store` method in this class.
	 * 
	 * @param {ORMEntity|ORMEntity[]} entities 
	 * @param  {...any} parameters 
	 * @returns {[{inserts: ORMEntity[], updates: ORMEntity[]}, ...any]}
	 */
	getStoreCommand(entities, ...parameters) {
		const inputData = Array.isArray(entities) ? entities : [entities];

		const { inserts, updates } = inputData.reduce((result, entity, index) => {
			if (entity instanceof this.EntityClass === false) {
				throw new TypeError(`Unexpected entity on index "${index}"`);
			}

			const primaryKeyValue = this.getEntityPrimaryKeyValue(entity);

			if (typeof primaryKeyValue === 'undefined' || primaryKeyValue === null) {
				result.inserts.add(entity)
			} else {
				result.updates.add(entity);
			}

			return result;
		}, { inserts: new Set(), updates: new Set() });

		return [{ inserts: [...inserts], updates: [...updates] }, ...parameters];
	}

	/**
	 * This method is overriden as original implementation cannot handle this case.
	 * As for mongo, updating and inserting, compared to SQL, is totally different.
	 * 
	 * @param {ORMEntity|ORMEntity[]} documents 
	 * @param  {...any} parameters 
	 * @returns {Promise<any>}
	 */
	async store(documents, ...parameters) {
		const entities = Array.isArray(documents) ? documents : [documents];
		const [{ inserts = [], updates = [] }] = this.getStoreCommand(entities, ...parameters);
		const [options = null] = parameters || [];

		const promises = [];

		if (inserts.length) {
			promises.push(this.storage.store({
				collection: `${this.source}`,
				command: MongoDBStorageHandler.Commands.InsertMany,
				payload: inserts.map((entity) => ({ ...entity })),
			}, options));
		}

		if (updates.length) {
			promises.push(...updates.map((entity) => this.storage.store({
				collection: `${this.source}`,
				command: MongoDBStorageHandler.Commands.FindOneAndUpdate,
				filters: { [`${entity.primaryKey}`]: this.getEntityPrimaryKeyValue(entity) },
				payload: { $set: { ...entity } },
			}, options)))
		}

		return Promise.all(promises);
	}
}

module.exports = MongoDBRepository;
