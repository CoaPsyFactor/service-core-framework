const { escape, escapeId } = require('mysql');

const AbstractClass = require('../../../utils/AbstractClass');
const MySQLStorageHandler = require('../../storage/handlers/mysql/MySQLStorageHandler');
const ORMEntity = require('../ORMEntity');
const ORMRepository = require('../ORMRepository');

class MySQLRepository extends ORMRepository {
	constructor(options) {
		super({ ...options, storageClass: MySQLStorageHandler });

		AbstractClass.call(this, MySQLRepository);
	}

	/**
	 * Build command used to fetch entities- result of this is propagated to the storage handler/driver
	 * 
	 * @param {string|object} criteria 
	 * @param  {...any} parameters 
	 * @returns {[string, ...any]}
	 */
	getFetchCommand(criteria, ...parameters) {
		if (typeof criteria === 'string') {
			return [`${criteria}`, ...parameters];
		}

		if (typeof criteria === 'object' && criteria !== null) {
			let query = `SELECT * FROM ${escapeId(this.source)}`;

			const criteriaEntries = Object.entries(criteria);

			if (criteriaEntries.length) {
				query = `${query} WHERE ${criteriaEntries.map(([field, value]) => `${escapeId(field)} = ${escape(value)}`).join(' AND ')}`
			}

			return [`${query};`, ...parameters];
		}

		throw new TypeError('Invalid fetch criteria.');
	}

	/**
	 * Build save (store) command from given input.
	 * This may result with multiple update queries mixed with single insert query.
	 * 
	 * @param {ORMEntity|ORMEntity[]} input 
	 * @param  {...any} parameters 
	 * @returns {[string, ...any]}
	 */
	getStoreCommand(input, ...parameters) {
		const inputData = Array.isArray(input) ? input : [input];

		/** @type {ORMEntity} */
		const entity = inputData[0] || null;

		if (entity instanceof ORMEntity === false) {
			throw new TypeError('Invalid input provided.');
		}

		const properties = entity.properties.reduce((result, property) => {
			if (property !== entity.primaryKey) {
				return [...result, property];
			}

			return [...result];
		}, []);

		const { inserts, updates } = inputData.reduce((result, entity) => {
			const primaryKeyValue = this.getEntityPrimaryKeyValue(entity);

			if (typeof primaryKeyValue === 'undefined') {
				result.inserts.push(properties.reduce((result, field) => {
					if (typeof entity[`${field}`] === 'undefined') {
						return [...result, 'NULL'];
					}

					return [...result, escape(entity[field])];
				}, []));
			} else {
				const values = properties.reduce((result, field) => {
					return [...result, `${escapeId(field)} = ${escape(entity[`${field}`])}`];
				}, []).join(', ');

				result.updates.add(`UPDATE ${escapeId(this.source)} SET ${values} WHERE ${escapeId(entity.primaryKey)} = ${escape(primaryKeyValue)}`)
			}

			return result;
		}, { inserts: [], updates: new Set() });

		const queriesBucket = [];

		if (inserts.length) {
			queriesBucket.push(`INSERT INTO ${escapeId(this.source)} (${[...properties].join(',')}) VALUES (${inserts.join('), (')})`);
		}

		if (updates.size) {
			queriesBucket.push(...updates);
		}

		return [queriesBucket.join(';'), ...parameters];
	}

	/**
	 * Generates delete query string from given input.
	 * 
	 * @param {ORMEntity|ORMEntity[]} entities 
	 * @param  {...any} parameters 
	 * @returns {[string, ...any]}
	 */
	getDeleteCommand(entities, ...parameters) {
		const inputData = Array.isArray(entities) ? entities : [entities];

		const primaryKeyValues = inputData.reduce((result, entity, index) => {
			const entityPrimaryKeyValue = this.getEntityPrimaryKeyValue(entity);

			if (typeof entityPrimaryKeyValue === 'undefined') {
				throw new TypeError(`Missing primary key value for entity on index "${index}"`);
			}

			result.add(escape(entityPrimaryKeyValue));

			return result;
		}, new Set());

		return [`DELETE FROM ${escapeId(this.source)} WHERE ${escapeId(this.primaryKey)} IN (${[...primaryKeyValues].join(',')});`, ...parameters];
	}
}

module.exports = MySQLRepository;
