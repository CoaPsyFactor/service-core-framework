module.exports = function AbstractClass(ClassIdentifier) {
	if (this.constructor === ClassIdentifier) {
		throw new Error(`${this.constructor.name} is an abstract class`);
	}
};
