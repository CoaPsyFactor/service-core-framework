# Framework Documentation
---
# UNDER HEAVY DEVELOPMENT!!!

- 1 Service
	- 1.1 Service Handler
		- 1.1.1 HTTP(s) Service Handler Base
		- 1.1.2 WS Service Handler Base
- 2 Storage
	- 2.1 Storage Handler
	- 2.2 Storage Driver
	- 2.3 Core Handlers and Drivers
		- 2.3.1 MySQL Storage
		- 2.3.2 MongoDB Storage
- 3 ORM
	- 3.1 Entity
	- 3.2 Repository
	- 3.3 Core Repositories
		- 3.3.1 MySQL
		- 3.3.2 MongoDB
- Additional Stuff
	- AbstractClass


# 1 Service
---
This module is used to bring together all other parts of the application.
It will ensure that all Service Handlers (1.1) are booted and handled as expected.
It also ensures that all storage engines and repositories required for service to work
are properly instantiated and initialized (e.g connections established).
It also provides health check state of full service, as any interruptions between the service and its dependencies
is caught and stored (e.g disconnect from mysql), or if some of handlers fails to either boot or handle.

usage example

```js

class NotificationService extends Service {
	constructor() {
		super({
			// These are all handlers that will be booted
			// and started with the service- their lifecycle
			// is being monitored and will result in un-healthy
			// service if boot or handle process fails
			handlers: [
				{
					// These are actual handlers, they are meant to provide
					// actual logic of the service- core implements websocket and http(s) base handlers that can be extended.
					handler: NotificationWebApiHandler,
					options: { port: 9090 }, // these are ServiceHandler options in constructor
				},
				{
					handler: NotificationWebSocketHandler,
					options: { port: 9091 }
				}
			],
			// These are all storage handler that are available
			// through out all service handlers, lifecycle of these
			// is also monitored and any failed connection or mis-behavior
			// will result in un-healthy service.
			// This is also easy to reach from the service handler implementation itself.
			// To get certain storage handler use `this.storage[StorageClass.name]`
			//  this will throw an error in case that invalid storage handler is requested.
			storage: [
				{
					handler: MySQLStorageHandler,
					config: {
						user: 'my-user',
						password: 'noPassword1'
						database: 'notification_db',
					}
				},
				{
					handler: MongoDBStorageHandler,
					config: {
						database: 'NotificationDatabase',
					}
				}
			],
			// These are all repositories that are used by this service-
			// this will ensure that each one of them is instantiated and 
			// initialized with proper storage handler.
			// To access repository instances from service handler implementation
			// use `this.repository[RepositoryClass.name]` - in case that non existing
			// repository is requested error is thrown.
			// Note: repository might be restricted to specific storage handler
w			//  in case that wrong one is provided exception is thrown
			repositories: [
				{

					repository: NotificationsRepository,
					storage: MySQLStorageHandler,
				},
				{
					repository: AccessKeysRepository,
					storage: MongoDBStorageHandler,
				},
			]
		})
	}
}

new NotificationService().start().then(() => {
	console.log('Notification service started');
}).catch((error) => {
	console.error(error);

	console.error('Failed to start Notification service');
});

```

## 1.1 Service Handler
This is a base class and it is used to provide easy way of adding logical functionality to the service. It has direct access to the service that is handling it
using ``this.service``, beside that it has direct access to all instantiated storage handlers and repositories, to retrieve use ``this.storage[StorageClass.name]`` for storage (mysql, mongo, etc) and ``this.repository[RepositoryClass.name]`` to get a repository of specific type. Note that both would throw an exception if invalid (non registered) instance is requested.

To provide even easier approach to registering access points, there are two abstract service handlers that are used for different types of a service handler.

Those abstract classes are:
 - 1.1.1 HTTP(s) Service Handler Base
 - 1.1.2 WS Service Handler Base

### 1.1.1 HTTP(s) Service Handler Base
Use this service handler as base class to have easy way of registering and handling HTTP requests, it is based on ``express`` framework and provides unique approach of how controllers are organizes, how they respond to a request and how they access repos and storage instances.
By using this handler- health check endpoint is added `GET /{PREFIX}/health-check` and it reads its values from the `Service`

usage example

file structure
```
___
   |___ controllers/
   |   |_ .....
   |   |_ GetUserNotifications.js
   |
   |_ NotificationWebApiHandler.js
```


`./controllers/GetUserNotifications.js`
```javascript
/**
 * @param {Request} request ExpressJS Request object.
 * @param {(data: any, status: number) => void} respond 
 * @param {NotificationWebApiHandler} handler 
 */
module.exports = async function GetUserNotifications(request, respond, handler) {
	// ... authenticate user- etc etc
	const notificationsRepo = handler.repository[NotificationsRepository.name];
	const notifications = await notificationsRepo.fetch({ userId });

	respond([...notifications], 200);
};
```

``./NotificationWebApiHandler.js``
```javascript
const HttpServiceHandler = require('../../../../src/core/service/handlers/HttpServiceHandler');

const GetUserNotifications = require('./controllers/GetUserNotifications');

class NotificationWebApiHandler extends HttpServiceHandler {
	/**
	 * @param {object} input Input data.
	 * @param {Service} input.service Parent service on which this handler is being used.
	 * @param {object} [input.options=null]
	 */
	constructor({ service, options: { port } }) {
		// Note that we added `prefix` property.
		// This one is required by HttpServiceHandler as its used
		// to group routing under that- so any endpoint, no matter in which group
		// will always start with `/notifications`
		super({ service, options: { port, prefix: 'notifications' } });
	}

	async handle() {
		// we can also use `post`, `put`, `patch`, `delete`
		// HttpServiceHandler will ensure to also add proper `OPTIONS` response to any registered route
		this.get('/', GetUserNotifications); // this would be `GET /notifications`
	}
}

module.exports = NotificationWebApiHandler;

```
### 1.1.2 WS Service Handler Base
This is still under development- but its purpose is to provide easy approach to websockets and handlers for them.